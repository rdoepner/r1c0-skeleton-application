# Skeleton Application for Zend Framework 2 #
---------------------------------------------

This is a simple skeleton application including a simple skeleton module for [Zend Framwork 2](http://framework.zend.com/). It can be used as a good starting point for a MVC and module based project.

## Requirements ##
* Apache >= 2.4
* PHP >=5.5

## Installation ##

[Download](https://bitbucket.org/rdoepner/r1c0-skeleton-application/downloads) and unzip this repository.

Open a terminal and change to unzipped folder:

```
cd /path/to/unzipped/repository/
```

Install [composer](http://getcomposer.org/) if not already installed:

```
curl -s http://getcomposer.org/installer | php
```

Install dependencies using composer:

```
php composer.phar install
```

## Usage ##

After a successful installation you should be able to enter the URL e.g. `http://localhost/repository/public/` in your browser.